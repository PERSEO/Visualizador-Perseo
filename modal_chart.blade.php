<div class="modal  SST4" tabindex="-1" role="dialog" data-dismiss="modal" >
    <div class="modal-dialogs modal-lg" role="document" style="width: 100%; height: auto">
        <div class="modal-content">
        <div class="modal-body">
        <span class="hint-modal-lyric">*Clic en leyenda de gráfica para ocultar.</span>
        <b><center><span id="modalShow">• Mostrando Clorofila •</span></center></b>
        <input type="button" name="clor_a" onclick="changeData(perseoCart,'c')" class="btn btn-sm btn-success" value="Mostrar CLOR_a">
        <input type="button" name="sst" onclick="changeData(perseoCart,'s')" class="btn btn-sm btn-danger" value="Mostrar SST">
        <canvas id="perseoCart" class="draw-chart"  height="35"></canvas>

        <input type="hidden" name="cmin" id="cmin" value="0.1">
        <input type="hidden" name="cmax" id="cmax" value="1">
        <input type="hidden" name="canualAverage" id="canualAverage" value="0.26,0.49,0.77,0.40,0.37,0.29,0.84,0.95,0.65,0.35,0.28,0.54,0.97,0.65,0.55,0.84">
        <input type="hidden" name="cdata" id="cdata" value="0.49,0.67,0.76,0.26,0.25,0.14,0.88,0.98,0.38,0.54,0.77,0.56,0.44,0.89,0.74,0.24">

        <input type="hidden" name="smin" id="smin" value="22">
        <input type="hidden" name="smax" id="smax" value="30">        
        <input type="hidden" name="sanualAverage" id="sanualAverage" value="26,29,27,24,27,29,24,25,25,25,28,24,27,25,25,24">
        <input type="hidden" name="sdata" id="sdata" value="29,27,26,26,25,24,28,28,28,24,27,26,24,29,24,24">

        
        <script type="text/javascript">
                var ctx = document.getElementById("perseoCart").getContext('2d');
                var min = document.getElementById("cmin").value;
                var max = document.getElementById("cmax").value;
                var anualAverage = document.getElementById("canualAverage").value;
                var data  = document.getElementById("cdata").value;
                var perseoCart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: ["2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017"],
                        datasets: 
                        [{
                            label: 'Este Punto en el Tiempo',
                            backgroundColor: 'rgba(255, 9, 3, 0.2)',//255, 99, 132, 0.2
                            borderColor:'rgba(255, 49, 3,1)',//255,99,132,1
                            borderWidth: 1,
                            data: JSON.parse("[" + data + "]"),
                        },
                        {
                            label: 'Promedio Anual',
                            backgroundColor: 'rgba(51,55,55,0.1)',//151,187,205,0.2
                            borderColor:'rgba(151,187,205,1)', //151,187,205,1  51,55,55,1
                            borderWidth: 1,
                            data: JSON.parse("[" + anualAverage + "]"),
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: parseInt(min),
                                    max: parseInt(max)
                                }
                            }]
                        },
                        responsive: true,
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        }
                    }
                });
        </script>
        </div>
        </div>
    </div>
</div>