let Mapa = {
  Objeto:{
    init: function(div, center, zoom){
      this.mapa = new ol.Map({
        target: div,
        view: new ol.View({
          // projection: 'EPSG:4326',
          center: ol.proj.fromLonLat(center),
          zoom: zoom
        })
      });
      this.mapa.on('singleclick', Mapa.Funciones.obtenerLocalizacion);
    },
    mapa:{}
  },
  Capas : {
    init:function(capas,arbol,base,lista,activador){
      this.arbolCapas = arbol;
      this.baseCapas = base;
      this.listaCapas = lista;
      activador.onclick = Mapa.Capas.aparecerDesaparecerArbol;
      for(tipo in capas){
        this.agregarCapas(tipo,capas[tipo]);
      }
    },
    arbolCapas:'',
    baseCapas:'',
    listaCapas:'',
    capas:{},
    agregarCapas:function(tipo,capas){
      let source = ''
      for(capa in capas){
        if(tipo == 'capasBase'){
          if(capas[capa]['type'] == 'STAMEN'){
            source = new ol.source.Stamen({
              layer: capas[capa]['layer']
            });
          }else if(capas[capa]['type'] == 'OSM'){
            source = new ol.source.OSM({
              layer: capas[capa]['layer']
            });
          }
        }else if(tipo == 'capas'){
          source = new ol.source.TileWMS({
            url: capas[capa]['url'],
            params: capas[capa]['parametros'],
            serverType: capas[capa]['servidor']
          });
        }
        let layer = new ol.layer.Tile({
          source: source,
          visible: capas[capa]['visible']
        });
        layer.set('tipo',tipo);
        layer.set('nombre', capa);
        this.capas[capa] = layer;
        Mapa.Objeto.mapa.addLayer(this.capas[capa]);
        Mapa.Capas.anadirCapaArbol(this.capas[capa]);
      }
    },
    aparecerDesaparecerArbol:function(e){
      if(e.target.text == "keyboard_arrow_up"){
        e.target.classList.add("rotate180");
        Mapa.Capas.desaparecerArbol(Mapa.Capas.arbolCapas);
        setTimeout(function(){
          e.target.classList.remove('rotate180');
          e.target.text = "keyboard_arrow_down";
        },450);
      }else{
        e.target.classList.add("rotate180");
        Mapa.Capas.aparecerArbol(Mapa.Capas.arbolCapas);
        setTimeout(function(){
          e.target.classList.remove('rotate180');
          e.target.text = "keyboard_arrow_up";
        },450);
      }
    },
    aparecerArbol:function(e){
      if(e.classList.contains('fade-out-tree'))
        e.classList.remove('fade-out-tree');
      if(e.classList.contains('hide'))
        e.classList.remove('hide');
      e.classList.add('fade-in-tree');
      e.classList.remove('hide');
      setTimeout(function(){
        e.classList.remove('fade-in-tree');
      },450);
    },
    desaparecerArbol: function(e){
      if(e.classList.contains('fade-in-tree'))
        e.classList.remove('fade-in-tree');
      e.classList.add('fade-out-tree');
      setTimeout(function(){
        e.classList.add('hide');
        e.classList.remove('fade-out-tree');
      },450);
    },
    anadirCapaArbol:function(layer){
      let lista = '',li = document.createElement('li'),a = document.createElement('a'), nombreCapa = layer.get('nombre');
      layer.get('tipo') == 'capas' ? lista = Mapa.Capas.listaCapas : lista = Mapa.Capas.baseCapas;
      a.text = nombreCapa;
      a.id = nombreCapa;
      a.onclick = Mapa.Capas.prenderApagarCapa
      li.append(a);
      lista.append(li);
    },
    prenderApagarCapa: function(e){
      let id = e.target.getAttribute('id');
      for(capa in Mapa.Capas.capas){
        if(id == capa){
          if(Mapa.Capas.capas[capa].get('tipo') == 'capasBase'){
            for(c in Mapa.Capas.capas){
              if(Mapa.Capas.capas[c].get('tipo') == 'capasBase')
                Mapa.Capas.apagarCapa(Mapa.Capas.capas[c]);
            }
            Mapa.Capas.prenderCapa(Mapa.Capas.capas[capa]);
          }else{
            Mapa.Capas.capas[capa].getVisible() ? Mapa.Capas.apagarCapa(Mapa.Capas.capas[capa]) : Mapa.Capas.prenderCapa(Mapa.Capas.capas[capa]);
          }
        }
      }
    },
    prenderCapa: function(e){
      let elementoArbol = document.getElementById(e.get('nombre'));
      if(!elementoArbol.classList.contains('activa'))
        elementoArbol.classList.add('activa');
      e.setVisible(true);

    },
    apagarCapa: function(e){
      let elementoArbol = document.getElementById(e.get('nombre'));
      if(elementoArbol.classList.contains('activa'))
        elementoArbol.classList.remove('activa');
      e.setVisible(false);
    }
  },
  Funciones:{
    obtenerLocalizacion:function(e){
      let coordenadas = Mapa.Funciones.transformTo4326(e.coordinate);
      // let coordenadas = e.coordinate;
      Mapa.Mensajes.mensaje('¡Haz hecho clic en el mapa!','Latitud: ' + coordenadas[0] + ' y Longitud: ' + coordenadas [1],'info');
      return ;
    },
    transformTo4326: function(coordenadas){
      return ol.proj.transform(coordenadas,'EPSG:3857','EPSG:4326');
    }
  },
  Mensajes:{
    mensaje: function(titulo,texto,tipo,botones = []){
      swal({
        title: titulo,
        text: texto,
        icon:tipo,
        button: botones
      });
      return ;
    },
  }
};
