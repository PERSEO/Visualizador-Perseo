let capas = {
  capasBase: {
    'Terrain - STAMEN': {'type': 'STAMEN','layer': 'terrain', 'visible': true},
    'Watercolor - STAMEN': {'type': 'STAMEN','layer': 'watercolor', 'visible': false},
    'Open Street Map - OSM': {'type': 'OSM', 'layer':'osm', 'visible': false}
  },
  capas: {
      'Clorofila 4km': {'parametros': {'LAYERS':'cloro_4km'}, 'url':'/perseowms?','servidor':'mapserver','visible': false,'consultable':true},
      'Sea Surface Temperature': {'parametros': {'LAYERS':'sst'}, 'url':'/perseowms?','servidor':'mapserver','visible': false,'consultable':true},
      'Lotes Menchaca': {'parametros': {'LAYERS':'lotes'}, 'url':'/menchacawms?','servidor':'mapserver','visible': false,'consultable':true},
      'Red Hidrográfica (CONABIO)': {'parametros':{'LAYERS':'hidro4mgw'},'url':'http://www.conabio.gob.mx/informacion/explorer/wms?','servidor':'geoserver','visible':false,'consultable':false}
    // 'Nombre arbol capas (Fuente cuando aplique)': {'parametros': {'LAYERS':'capa'}, 'url':'url al recurso wms','servidor':'mapserver o geoserver'}
  }
}
