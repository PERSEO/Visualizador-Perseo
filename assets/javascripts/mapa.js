let Mapa = {
  Objeto:{
    init: function(div, center, zoom,minz,maxz){
      this.mapa = new ol.Map({
        target: div,
        view: new ol.View({
          // projection: 'EPSG:4326',
          center: ol.proj.fromLonLat(center),
          zoom: zoom,
          minZoom: minz,
          maxZoom: maxz
        })
      });
      this.mapa.on('singleclick', Mapa.Funciones.getFeatureInfo);
      // this.mapa.on('singleclick', Mapa.Funciones.obtenerLocalizacion);
    },
    mapa:{}
  },
  Capas : {
    init:function(capas,arbol,base,lista,activador){
      this.arbolCapas = arbol;
      this.baseCapas = base;
      this.listaCapas = lista;
      activador.onclick = Mapa.Capas.aparecerDesaparecerArbol;
      for(tipo in capas){
        this.agregarCapas(tipo,capas[tipo]);
      }
    },
    arbolCapas:'',
    baseCapas:'',
    listaCapas:'',
    capas:{},
    agregarCapas:function(tipo,capas){
      let source = ''
      for(capa in capas){
        if(tipo == 'capasBase'){
          if(capas[capa]['type'] == 'STAMEN'){
            source = new ol.source.Stamen({
              layer: capas[capa]['layer']
            });
          }else if(capas[capa]['type'] == 'OSM'){
            source = new ol.source.OSM({
              layer: capas[capa]['layer']
            });
          }
        }else if(tipo == 'capas'){
          // Falta la construcción de esta función
          source = new ol.source.TileWMS({
            url: capas[capa]['url'],
            params: capas[capa]['parametros'],
            serverType: capas[capa]['servidor']
          });
        }
        let layer = new ol.layer.Tile({
          source: source,
          visible: capas[capa]['visible']
        });
        layer.set('tipo',tipo);
        layer.set('nombre', capa);
        layer.set('consultable',capas[capa]['consultable']);
        this.capas[capa] = layer;
        Mapa.Objeto.mapa.addLayer(this.capas[capa]);
        Mapa.Capas.anadirCapaArbol(this.capas[capa]);
      }
    },
    aparecerDesaparecerArbol:function(e){
      if(e.target.text == "keyboard_arrow_up"){
        e.target.classList.add("rotate180");
        Mapa.Capas.desaparecerArbol(Mapa.Capas.arbolCapas);
        setTimeout(function(){
          e.target.classList.remove('rotate180');
          e.target.text = "keyboard_arrow_down";
        },450);
      }else{
        e.target.classList.add("rotate180");
        Mapa.Capas.aparecerArbol(Mapa.Capas.arbolCapas);
        setTimeout(function(){
          e.target.classList.remove('rotate180');
          e.target.text = "keyboard_arrow_up";
        },450);
      }
    },
    aparecerArbol:function(e){
      if(e.classList.contains('fade-out-tree'))
        e.classList.remove('fade-out-tree');
      if(e.classList.contains('hide'))
        e.classList.remove('hide');
      e.classList.add('fade-in-tree');
      e.classList.remove('hide');
      setTimeout(function(){
        e.classList.remove('fade-in-tree');
      },450);
    },
    desaparecerArbol: function(e){
      if(e.classList.contains('fade-in-tree'))
        e.classList.remove('fade-in-tree');
      e.classList.add('fade-out-tree');
      setTimeout(function(){
        e.classList.add('hide');
        e.classList.remove('fade-out-tree');
      },450);
    },
    anadirCapaArbol:function(layer){
      let lista = '',li = document.createElement('li'),a = document.createElement('a'), nombreCapa = layer.get('nombre');
      layer.get('tipo') == 'capas' ? lista = Mapa.Capas.listaCapas : lista = Mapa.Capas.baseCapas;
      a.text = nombreCapa;
      a.id = nombreCapa;
      a.onclick = Mapa.Capas.prenderApagarCapa
      li.append(a);
      lista.append(li);
    },
    prenderApagarCapa: function(e){
      let id = e.target.getAttribute('id');
      for(capa in Mapa.Capas.capas){
        if(id == capa){
          if(Mapa.Capas.capas[capa].get('tipo') == 'capasBase'){
            for(c in Mapa.Capas.capas){
              if(Mapa.Capas.capas[c].get('tipo') == 'capasBase')
                Mapa.Capas.apagarCapa(Mapa.Capas.capas[c]);
            }
            Mapa.Capas.prenderCapa(Mapa.Capas.capas[capa]);
          }else{
            Mapa.Capas.capas[capa].getVisible() ? Mapa.Capas.apagarCapa(Mapa.Capas.capas[capa]) : Mapa.Capas.prenderCapa(Mapa.Capas.capas[capa]);
          }
        }
      }
    },
    prenderCapa: function(e){
      let elementoArbol = document.getElementById(e.get('nombre'));
      if(!elementoArbol.classList.contains('activa'))
        elementoArbol.classList.add('activa');
      e.setVisible(true);

    },
    apagarCapa: function(e){
      let elementoArbol = document.getElementById(e.get('nombre'));
      if(elementoArbol.classList.contains('activa'))
        elementoArbol.classList.remove('activa');
      e.setVisible(false);
    },
    obtenerCapasActivas: function(){
      let capasActivas = {};
      for(capa in Mapa.Capas.capas){
        if(Mapa.Capas.capas[capa].get('tipo') == 'capas' && Mapa.Capas.capas[capa].get('consultable') && Mapa.Capas.capas[capa].getVisible()){
          capasActivas[capa] = Mapa.Capas.capas[capa].get('nombre');
        }
      }
      return capasActivas;
    },
  },
  Funciones:{
    obtenerLocalizacion:function(coordenadas){
      // let div = document.createElement('div');
      // div.innerHTML = ;
      Mapa.Mensajes.mensaje('¡Haz hecho clic en el mapa!','<b>Latitud</b>: ' +  coordenadas[0].toFixed(4) + ' & <b>Longitud</b>: ' + coordenadas [1].toFixed(4),false,["Cerrar", "Ver Gráfico"]);
      return ;
    },
    transformTo4326: function(coordenadas){
      return ol.proj.transform(coordenadas,'EPSG:3857','EPSG:4326');
    },
    getFeatureInfo:function(e){
      let coordenadas = Mapa.Funciones.transformTo4326(e.coordinate), capasActivas = Mapa.Capas.obtenerCapasActivas(), capas = '',i=0;
      if(Object.keys(capasActivas).length > 0){
        for(capa in capasActivas){
          if(i>0)
            capas += ','
          capas += capasActivas[capa];
          i++;
        }
        Mapa.Utilidades.Ajax.enviarPeticion(Mapa._url_ + 'ajax/gfi.json?' + 'coordenadas=' + coordenadas[0] + ',' + coordenadas[1] + '&capas=' + capas,Mapa.Funciones.mostrarDatos);
      }else{
        Mapa.Funciones.obtenerLocalizacion(coordenadas);
      }
      return ;
    },
    mostrarDatos:function(){
      let datos = Mapa.Utilidades.Ajax.datos,texto='<tbody>',encabezados = '<thead><tr><th>Capas</th><th>Valor</th></thead>';
      for(dato in datos){
        Object.keys(datos[dato]).forEach(function(k){
          Object.keys(datos[dato][k]).forEach(function(l){
            datos[dato][k][l]['val'] != null ? valor = datos[dato][k][l]['val'].toFixed(4) : valor = 'No hay valor';
            texto += '<tr><td>' + k +'</td><td>' + valor + '</td></tr>';
          });
        });
      }
      texto += '</tbody>';
      Mapa.Mensajes.mensaje('Identificador de Objetos','<table class="table table-striped table-hover">'+encabezados+texto+'</table>',false,["Cerrar", "Ver Gráfico"]);
    }
  },
  Mensajes:{
    mensaje: function(titulo,texto,tipo,botones = []){
      let contenido = document.createElement('div');
      contenido.innerHTML = texto;
      swal({
        title: titulo,
        content: contenido,
        icon:tipo,
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: {
          cancel: botones[0],
          chart: botones[1]
        },
      }).then((value)=>{
        if(value==="chart"){
          document.getElementById("chart_active").click();
        }
      });
    },
  },
  Utilidades:{
    Ajax:{
      enviarPeticion(url,accion){
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function(){
          if(this.readyState == 4 && this.status == 200){
            Mapa.Utilidades.Ajax.datos = JSON.parse(this.response);
            accion();
          }
        };
        ajax.open('GET',url,true);
        ajax.send();
      },
      datos:{}
    },
  },
  _url_: 'http://localhost/perseobackend/',
}
