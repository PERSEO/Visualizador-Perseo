function changeData(chart,target){
    chart.options.scales.yAxes[0].ticks.min=parseInt(document.getElementById(target+"min").value);//Reajustar mínimo en escala de chart
    chart.options.scales.yAxes[0].ticks.max=parseInt(document.getElementById(target+"max").value);//Reajustar máximo en escala de chart
    var totalData = chart.data.datasets[1].data.length; //Calcular número de años
    var newAverage = JSON.parse("[" + document.getElementById(target+"anualAverage").value + "]");//Nuevo valor promedio anual
    var newData = JSON.parse("[" + document.getElementById(target+"data").value + "]");//Nuevo valor de data 
    for (var i = 0; i < totalData; i++) {//Eliminar dato por cada año
        chart.data.datasets[0].data.pop();
        chart.data.datasets[1].data.pop();
    }
    chart.update();//Actualzar data en chart
    for (var i = 0; i < totalData; i++) {//Agregar dato por cada año
        chart.data.datasets[0].data.push(newData[i]);
        chart.data.datasets[1].data.push(newAverage[i]);
    }
    chart.update();//Actualizar data
    if(target==='c'){
        document.getElementById("modalShow").innerHTML="• Mostrando Clorofila •";
    }else{
        document.getElementById("modalShow").innerHTML="• Mostrando Temperatura •";
    }
}